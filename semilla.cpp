
#include "semilla.h"

Semilla::Semilla() {
	nombreCultivo=' ';
	costoSemilla=0;
	tiempoCosecha=0;
	rentabiliidad=0;
	tiempoRecuperacion=0;
	aguaPorTurno=0;

}
Semilla::Semilla(const Semilla& semillaACopiar){
	this->nombreCultivo=semillaACopiar.nombreCultivo;
		this->costoSemilla=semillaACopiar.costoSemilla;
		this->tiempoCosecha=semillaACopiar.tiempoCosecha;
		this->rentabiliidad=semillaACopiar.rentabiliidad;
		this->tiempoRecuperacion=semillaACopiar.tiempoRecuperacion;
		this->aguaPorTurno=semillaACopiar.aguaPorTurno;
}
Semilla::Semilla(char nombre, Entero costo, Entero tiempoCosecha, Entero rentabilidad, Entero tiempoRecuperacion, Entero aguaPorTurno){
	this->nombreCultivo=nombre;
	this->costoSemilla=costo;
	this->tiempoCosecha=tiempoCosecha;
	this->rentabiliidad=rentabilidad;
	this->tiempoRecuperacion=tiempoRecuperacion;
	this->aguaPorTurno=aguaPorTurno;
}

char Semilla::consultarNombre(){
	return this->nombreCultivo;
}

Entero Semilla::consultarCosto(){
	return this->costoSemilla;
}


Entero Semilla::consutlarTiempoCosecha(){
	return this->tiempoCosecha;
}

Entero Semilla::consultarRentabilidad(){
	return this->rentabiliidad;
}

Entero Semilla::consultartiempoRecuperacion(){
	return this->tiempoRecuperacion;
}

Entero Semilla::consultarAguaPorTurno(){
	return this->aguaPorTurno;
}
bool Semilla::sonIguales(Semilla semilla){
	return semilla.consultarNombre()==this->consultarNombre();
}
