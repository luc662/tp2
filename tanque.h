#ifndef TANQUE_H_
#define TANQUE_H_
typedef unsigned int Entero;

class Tanque{
	private:
		Entero cargaTanque;
		Entero capacidadTanque;
		Entero costoMejora;
		Entero tamanioInicial;
		Entero aguaTotal;
	public:
		//crea el tanque si no se pasa parametros
	Tanque();
	//post: crea el tanque dependiendo de los paramentros pasados.
	Tanque(Entero n,Entero m, double Multiplicador);

	//post:devuelve la capacidad del tanque.
	Entero consultarCapacidad();

	//post: devuelve  la carga usada del tanque
	Entero consultarCarga();

	//post: devuelve el costo de mejorar el tanque.
	Entero consultarCostoDeMejora();

	//post: devuelve el tamanio que tenia el tanque al momento de su creacion.
	Entero consultarTamanioInicial();

	//post:devuelve la cantidad de agua total.
	Entero consultarAguaTotal();

	//post: aumenta la capacidad y el costo de mejorar el tanque y devuelve el costo de la mejora.
	Entero MejorarTanque(float multiplicador);

	//pre: se ingresa una cantidad valida de agua.
	//post: devuelve si se dispone de la cantidad de agua pasada por parametro.
	bool sePuedeUsarAgua(Entero agua);

	//post: aumenta la cantidad de aguaTotal del tanque, si el tanque no esta lleno, tambien aumenta la cantidad de agua
	void cargarAgua(Entero agua);

	//pre: no se va a usar mas augua que la que se dispone.
	//post: disminuye la cantidad de agua total, y si se usa mas agua de la que sobra se usa tambien agua del tanque.
	void descargarAgua(Entero agua);


	//post: se tira toda el agua que no entre en el tanque.
	void tirarSobrante();
};


#endif /* TANQUE_H_ */
