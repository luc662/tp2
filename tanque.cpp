#include "tanque.h"

 Tanque::Tanque(){
	this->costoMejora=0;
	this->cargaTanque=0;
	this->capacidadTanque=0;
	this->tamanioInicial=this->capacidadTanque;
	this-> aguaTotal=0;
 }

 Tanque::Tanque (Entero n,Entero m, double Multiplicador){
 	this->costoMejora=(n*m)*Multiplicador;
	this->cargaTanque=0;
	this->capacidadTanque=(n*m)*Multiplicador;;
	this->tamanioInicial=this->capacidadTanque;
	this->aguaTotal=0;
 }
 Entero Tanque::consultarCapacidad(){
	 return this->capacidadTanque;
 }
 Entero Tanque::consultarCarga(){
	 return this -> cargaTanque;
 }

 Entero Tanque::consultarCostoDeMejora(){
 	 return this->costoMejora;
  }

  Entero Tanque::consultarTamanioInicial(){
 	 return this->tamanioInicial;
  }
  Entero Tanque::consultarAguaTotal(){
	return this->aguaTotal;
  }

bool Tanque::sePuedeUsarAgua (Entero agua){
	return agua <= this->consultarAguaTotal();
}

void Tanque::cargarAgua(Entero agua){
 	this->aguaTotal+=agua;
	if(this->capacidadTanque<=(this->cargaTanque+agua)){
		this->cargaTanque=this->capacidadTanque;
	}
	else cargaTanque+=agua;
}

Entero Tanque::MejorarTanque(float multiplicador){
	Entero costoViejo=this->costoMejora;
	this->costoMejora=(this->costoMejora+this->tamanioInicial)*multiplicador;
	this->capacidadTanque+=this->tamanioInicial;
	return costoViejo;
}

void Tanque::descargarAgua(Entero agua){
	this->aguaTotal-=agua;
	if(this->aguaTotal<this->cargaTanque)
		this->cargaTanque=this->aguaTotal;
}
