##ifndef SEMILLA_H_
#define SEMILLA_H_
typedef unsigned int Entero;

class Semilla {
private:
	char nombreCultivo;
	Entero costoSemilla;
	Entero tiempoCosecha;
	Entero rentabiliidad;
	Entero tiempoRecuperacion;
	Entero aguaPorTurno;
public:

	Semilla();
	//post: crea una semilla igual a la que se paso por referencia
	Semilla(const Semilla& semillaACopiar);

	Semilla(char nombre, Entero costo, Entero tiempoCosecha, Entero rentabilidad, Entero tiempoRecuperacion, Entero aguaPorTurno);
	//post: devuelve el nombre de la semilla.
	char consultarNombre();
	//post: devuelve el costo de la semilla.
	Entero consultarCosto();
	//post: devuelve el tiempo de cosecha de la semilla.
	Entero consutlarTiempoCosecha();
	//post: devuelve la rentabilidad de la semilla.
	Entero consultarRentabilidad();
	//post: devuelve el tiempo de recuperacion de la semilla.
	Entero consultartiempoRecuperacion();
	//post: devuelve el costo de agua de la semilla.
	Entero consultarAguaPorTurno();
	//post: devuelve si la semilla tiene el mismo nombre que la que se paso por valor.
	 bool sonIguales(Semilla semilla);
};

#endif /* SEMILLA_H_ */